<?php namespace ProcessWire;
class PagefieldPairsConfig extends ModuleConfig {
    public function __construct() {
        $this->add(array(
            array(
                'name' => 'pairs',
                'type' => 'textarea', 
                'label' => $this->_('Pagefield Pairs'),
                'description' => $this->_('Setup your Field Parings here. One by line. Syntax: template:field|template:field'), 
                'notes' => $this->_('Sync teams of a player and players of a team would be "playertemplate:teamsfield|teamstemplate:playersfield"'),
            )
        ));
    }
}